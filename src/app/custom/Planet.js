import EventEmitter from "eventemitter3";
import config from "../../config";
import delay from "../utils";
import Person from "./Person";
import StarWarsUniverse from "./StarWarsUniverse";

export default class Planet extends EventEmitter {
  constructor(name, populationDelay, peopleData) {
    super();
    this.name = name;
    this.config = {populationDelay: populationDelay}
    this.peopleData = peopleData;
    this.population = [];
  }

  static get events() {
    return {
      PERSON_BORN: "person_born",
      POPULATING_COMPLETED: "populating_completed",
    };
  }
  
  get populationCount() {
    return this.population.length
  }
  async populate() {
    if(!this.peopleData.length) {
      this.emit(Planet.events.POPULATING_COMPLETED)
      return;
    };
    await delay(this.config.populationDelay);
    let personData = this.peopleData.pop();
    const person = new Person(personData.name, personData.height, personData.mass);
    this.population.push(person);
    this.emit(Planet.events.PERSON_BORN, personData.films)
    this.populate();
  }
}
