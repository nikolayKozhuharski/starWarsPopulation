import EventEmitter from "eventemitter3";
import Film from "./Film";
import Planet from "./Planet";

export default class StarWarsUniverse extends EventEmitter {
  constructor() {
    super();
    this.films = [];
    this.planet = null;
  }
  static get events() {
    return {
      FILM_ADDED: "film_added",
      UNIVERSE_POPULATED: "universe_populated",
    };
  }
  async init() {
    let response = await fetch("https://swapi.booost.bg/api/planets/");
    let planets = await response.json();
    let peopleResponse = await fetch("https://swapi.booost.bg/api/people/");
    let people = await peopleResponse.json();
    let populationZero = planets.results.filter(
      (p) => p.population === "unknown"
    );

    this.planet = new Planet(
      populationZero[0].name,
      1,
      await this.numberOfPeople(10, people)
    );
    this.planet.on(Planet.events.PERSON_BORN, (filmUrls) => {
      this._onPersonBorn(filmUrls);
    });
    this.planet.on(Planet.events.POPULATING_COMPLETED, () => {
      this.emit(StarWarsUniverse.events.UNIVERSE_POPULATED);
    });
    // this.on("universe_populated", ( ) => {
    //   console.log(this.films)
    // })
    this.planet.populate();
  }
  _onPersonBorn(filmUrls) {
    for (const el of filmUrls) {
      const curentEl = this.films.filter((e) => e.url === el);
      if (curentEl.length) {
        continue;
      } else {
        const film = new Film(el);
        this.films.push(film);
        this.emit(StarWarsUniverse.events.FILM_ADDED);
      }
    }
    
  }

  async numberOfPeople(numberOfPeople, fetchData) {
    const people = [];
    for (let i = 0; i < numberOfPeople; i++) {
      let person = await fetchData.results[i];
      people.push(person);
    }
    return people;
  }
}
