/**
 * Here you can define helper functions to use across your app.
 */
export default async function delay(sec) {
  return new Promise((resolve) => setTimeout(resolve, sec * 1000));
}